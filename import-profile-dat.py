#Author-dag.rende@gmail.com
#Description-import wing profile dat file and make it a bezier curve

# data file spec: https://aerofoilengineering.com/Help/HelpDataFiles.htm
# dat example: https://m-selig.ae.illinois.edu/ads/coord/mh64.dat

import adsk.core, adsk.fusion, adsk.cam, traceback, os, re

def run(context):
    ui = None
    try:
        app = adsk.core.Application.get()
        ui  = app.userInterface

        points = adsk.core.ObjectCollection.create()

        def addPoint(x, y):
            p1 = adsk.core.Point3D.create(x, y)
            points.add(p1)

        sketch = app.activeEditObject
        if not type(sketch) is adsk.fusion.Sketch:
            ui.messageBox("Only works in an open sketch")
            return None

        wantedLength = 12

        fileDialog = ui.createFileDialog()
        fileDialog.isMultiSelectEnabled = False
        fileDialog.title = "Select wing profile"
        fileDialog.filter = 'Text files (*.dat)'
        fileDialog.filterIndex = 0
        dialogResult = fileDialog.showOpen()
        if dialogResult == adsk.core.DialogResults.DialogOK:
            filename = fileDialog.filename
        else:
            return

        airFoil = AirFoil(filename)

        log(f"airFoil.points={airFoil.points}")

        # scale to wanted length
        datPoints = list(map(lambda p: [p[0] * wantedLength, p[1] * wantedLength], list(airFoil.points)))
        log(f"datPoints={datPoints}")

        cg = findCG(datPoints)
        log(f"cg={cg}")

        minx = maxx = cg[0]
        for point in datPoints:
            minx = min(minx, point[0])
            maxx = max(maxx, point[0])
        log(f"minmaxx={[minx, maxx]}")
        # make wing point left
        # if abs(cg[0] - minx) > abs(cg[0] - maxx):
        #     datPoints = map(lambda p: [maxx - p[0], p[1]], datPoints)
        #     cg[0] = maxx - cg[0]

        for point in datPoints:
            addPoint(float(point[0]), float(point[1]))

        # createClosePolygonFromPoints(sketch, points)
        sketch.sketchCurves.sketchFittedSplines.add(points)

        cgp = adsk.core.Point3D.create(cg[0], cg[1])
        log(f"cgpoint={[cgp.x, cgp.y]}")
        sketch.sketchPoints.add(adsk.core.Point3D.create(cg[0], cg[1]))
    except:
        if ui:
            ui.messageBox('Failed:\n{}'.format(traceback.format_exc()))

# add the curve of straight lines between the points to sketch
def createClosePolygonFromPoints(sketch, points):
    lines = sketch.sketchCurves.sketchLines
    pt1 = points.item(0)
    pt2 = points.item(1)
    firstLine = lines.addByTwoPoints(pt1, pt2)
    lastLine = firstLine
    for i in range(1,points.count-1):
        pt1 = lastLine.endSketchPoint
        pt2 = points.item(i+1)
        lastLine = lines.addByTwoPoints(pt1, pt2)
    pt1 = lastLine.endSketchPoint
    pt2 = firstLine.startSketchPoint
    lines.addByTwoPoints(pt1, pt2)

# reads dat file into title and array of 2 el cord array
def readFileIntoCoordArray(filePath):
    points = []
    with open(filePath, 'r') as file:
        title = file.readline()
        line = file.readline()
        while line:
            line = line.strip()
            if len(line) > 0:
                lineParts = line.split()
                points.append((float(lineParts[0]), float(lineParts[1])))
            line = file.readline()
    return (title, points)

class AirFoil:
    # read file into a new AirFoil instance
    # file can be of two formats:
    # Simple: title row followed by row with x y pars of floats, each between 0 and 1
    # Lednicer: title row followed by two integer numbers denoting the number of upper and lower side coords
    #   row with pairs for upper side followed by rows for lower side - both sides beginning in back end of wing
    # in both cases blank lines are ignored
    def __init__(self, filePath):
        self.points = []
        with open(filePath, 'r') as file:
            self.title = file.readline().strip()
            pair = self.readPair(file)
            log(f"title={self.title} pair={pair}")
            if abs(pair[0]) < 1.1:
                # Simple format
                log("Simple")
                while pair:
                    self.points.append(pair)
                    pair = self.readPair(file)
            else:
                # Lenicer format
                log("Lenicer")
                upperLowerCount = pair

                # read upper side points
                pair = self.readPair(file)
                log(f"{pair}")
                while pair and len(self.points) < upperLowerCount[0]:
                    self.points.append(pair)
                    pair = self.readPair(file)
                    log(f"{pair}")
                if len(self.points) != upperLowerCount[0]:
                    log(f"wrong nbr of upper points: {len(self.points)} - should have been {upperLowerCount[0]}")
                    raise f"wrong nbr of upper points: {len(self.points)} - should have been {upperLowerCount[0]}"
                
                # read lower side points
                lowerPoints = []
                log(f"first lower pair={pair}")
                while pair:
                    lowerPoints.append(pair)
                    pair = self.readPair(file)
                if len(lowerPoints) != upperLowerCount[1]:
                    log(f"wrong nbr of lower points: {len(lowerPoints)} - should have been {upperLowerCount[1]}")
                    raise f"wrong nbr of lower points: {len(lowerPoints)} - should have been {upperLowerCount[1]}"
                log(f"lowerPoints={lowerPoints}")
                lowerPoints.reverse()
                self.points = self.points + lowerPoints
                log("end lower pairs")

    def readPair(self, file):
        line = file.readline()
        # log(f"readPair line={line}")
        if not line:
            return None
        while len(line.strip()) == 0:
            line = file.readline()
            # log(f"readPair while line={line}")
            if not line:
                return None
        # remove decimal points without decimals - ex change "49. " to "49 "
        line = re.sub("\\.([^0-9]|$)", "\\g<1>", line, 0)
        log(f"readPair line={line}")
        pair = line.strip().split()
        return (float(pair[0]), float(pair[1]))



# Returns the cg in CG.  Computes the weighted sum of
# each triangle's area times its centroid.  Twice area
# and three times centroid is used to avoid division
# until the last moment.
def findCG(P):
    CG0 = 0.0
    CG1 = 0.0
    areasum2 = 0.0
    for i in range(1,  len(P) - 1):
        cent3 = centroid3(P[0], P[i], P[i + 1])
        A2 =  area2(P[0], P[i], P[i + 1])
        CG0 += A2 * cent3[0]
        CG1 += A2 * cent3[1]
        areasum2 += A2
    return [CG0 / (3 * areasum2), CG1 / (3 * areasum2)]

# Returns twice the signed area of the triangle determined by a,b,c,
# positive if a,b,c are oriented ccw, and negative if cw.
def area2(a, b, c):
	return (b[0] - a[0]) * (c[1] - a[1]) - (c[0] - a[0]) * (b[1] - a[1])

# Returns three times the centroid.  The factor of 3 is
# left in to permit division to be avoided until later.
def centroid3(p1, p2, p3):
    return [p1[0] + p2[0] + p3[0], p1[1] + p2[1] + p3[1]]

def log(msg):
    try:
        with open(os.path.join(os.path.dirname(os.path.abspath(__file__)), 'log.txt'), 'a+') as file:
            file.write(msg + "\n")
    except:
        pass

